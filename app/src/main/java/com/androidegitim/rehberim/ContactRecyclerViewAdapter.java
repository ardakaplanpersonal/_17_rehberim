package com.androidegitim.rehberim;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Arda Kaplan on 4.01.2018 - 15:58
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class ContactRecyclerViewAdapter extends RecyclerView.Adapter<ContactRecyclerViewAdapter.ContactViewHolder> {

    private Activity activity;
    private ArrayList<Contact> contactArrayList;

    public ContactRecyclerViewAdapter(Activity activity, ArrayList<Contact> contactArrayList) {

        this.activity = activity;
        this.contactArrayList = contactArrayList;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        View view = layoutInflater.inflate(R.layout.row_contact, parent, false);

        return new ContactViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ContactViewHolder holder, int position) {

        final Contact contact = contactArrayList.get(position);

        holder.nameTextView.setText(contact.getName());

        holder.rootLinearLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                ContactDialog contactDialog = new ContactDialog(activity);

                contactDialog.setContact(contact);

                contactDialog.show();

            }
        });

    }

    @Override
    public int getItemCount() {
        return contactArrayList.size();
    }

    class ContactViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.row_contact_linearlayout_root)
        LinearLayout rootLinearLayout;
        @BindView(R.id.row_contact_textview_name)
        TextView nameTextView;


        ContactViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }

    }
}
