package com.androidegitim.rehberim;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

/**
 * Created by Arda Kaplan on 4.01.2018 - 16:22
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class ContactDialog extends Dialog {

    public ContactDialog(@NonNull Context context) {
        super(context);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.dialog_contact);

        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

//        setCancelable(false);
//
//        setCanceledOnTouchOutside(false);
    }

    public void setContact(final Contact contact) {

        ((TextView) findViewById(R.id.dialog_contact_textview_contact_name)).setText(contact.getName());

        findViewById(R.id.dialog_contact_imageview_sms).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent smsIntent = new Intent(Intent.ACTION_VIEW);

                smsIntent.setData(Uri.parse("sms: " + contact.getPhoneNo()));

                smsIntent.putExtra("sms_body", "merhaba");

                getContext().startActivity(smsIntent);

            }
        });

        findViewById(R.id.dialog_contact_imageview_call).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_VIEW);

                callIntent.setData(Uri.parse("tel: " + contact.getPhoneNo()));

                getContext().startActivity(callIntent);

            }
        });
    }
}
