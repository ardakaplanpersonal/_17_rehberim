package com.androidegitim.rehberim;

import android.support.annotation.NonNull;

/**
 * Created by Arda Kaplan on 4.01.2018 - 15:52
 * <p>
 * arda.kaplan09@gmail.com
 * <p>
 * www.ardakaplan.com
 */

public class Contact implements Comparable<Contact> {

    private String name;
    private String phoneNo;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    @Override
    public int compareTo(@NonNull Contact anotherContact) {
        return getName().compareTo(anotherContact.getName());
    }
}
