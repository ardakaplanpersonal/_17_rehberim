package com.androidegitim.rehberim;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidegitim.androidegitimlibrary.helpers.DialogHelpers;
import com.androidegitim.androidegitimlibrary.helpers.RDALogger;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ContactsActivity extends AppCompatActivity {


    @BindView(R.id.main_recyclerview_contacts)
    RecyclerView contactsRecyclerView;

    private final int PERMISSION_REQUEST_CONTACT = 458;

    private ArrayList<Contact> contactArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        ButterKnife.bind(this);

        RDALogger.start(getString(R.string.app_name)).enableLogging(true);

        askPermission();
    }


    private void askPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)) {

                    DialogHelpers.showDialog(ContactsActivity.this, "Rehbere erişim izni gerekli!", "REHBERİM uygulamasını kullanmak için izin vermelisiniz.",
                                             "Tamam", new DialogInterface.OnClickListener() {
                                @TargetApi(Build.VERSION_CODES.M)
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();

                                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSION_REQUEST_CONTACT);
                                }
                            }, "Hayır", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    dialog.dismiss();

                                    Toast.makeText(getApplicationContext(), "İzin vermediniz.", Toast.LENGTH_SHORT).show();
                                }
                            }, null, null);

                } else {

                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_CONTACTS}, PERMISSION_REQUEST_CONTACT);
                }

            } else {

                getContactList();
            }
        } else {

            getContactList();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == PERMISSION_REQUEST_CONTACT && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            getContactList();
        }
    }

    /**
     * rehberi okuyan metot
     */
    private void getContactList() {

        ContentResolver contentResolver = getContentResolver();

        Cursor cursor = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        if ((cursor != null ? cursor.getCount() : 0) > 0) {

            while (cursor != null && cursor.moveToNext()) {

                String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

                String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));

                if (cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)) > 0) {

                    Cursor pCur = contentResolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                                                        null,
                                                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                                                        new String[]{id}, null);

                    while (pCur.moveToNext()) {

                        String phoneNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

                        Contact contact = new Contact();
                        {
                            contact.setName(name);

                            contact.setPhoneNo(phoneNo);

                            contactArrayList.add(contact);
                        }
                    }

                    pCur.close();
                }
            }
        }

        if (cursor != null) {

            cursor.close();
        }

        fillList();
    }

    private void fillList() {

        Collections.sort(contactArrayList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());

        linearLayoutManager.setOrientation(LinearLayout.VERTICAL);

        contactsRecyclerView.setLayoutManager(linearLayoutManager);

        contactsRecyclerView.setAdapter(new ContactRecyclerViewAdapter(ContactsActivity.this, contactArrayList));

    }
}
